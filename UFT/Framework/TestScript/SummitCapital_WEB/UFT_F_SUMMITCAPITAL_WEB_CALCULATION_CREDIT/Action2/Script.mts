﻿'Get Parameter Data
inputFileData = Parameter.Item("INPUT_FILE_DATA")
currentRowData = Parameter.Item("CURRENT_ROW")
workSpaceData = Parameter.Item("WORKSPACE")
outputSheetNameData = Parameter.Item("OUTPUT_SHEET_NAME")

'Load Object Repository
RepositoriesCollection.Add workSpaceData & "ObjectRepository\SummitCapital_WEB\SUMMITCAPITAL_CAL_CREDIT_Repository.tsr" 

inputSheetData = "INPUT"
outputFileData = inputFileData
outputSheetData = outputSheetNameData

'Call FW_WriteLog("inputFileData", inputFileData)
'Call FW_WriteLog("inputSheetData", inputSheetData)
'Call FW_WriteLog("outputFileData", outputFileData)
'Call FW_WriteLog("outputSheetData", outputSheetData)

'Call FW_WriteLog("Set CurrentRow", currentRowData)
'Read Excel TestData
Call FW_GetTestData(inputFileData, inputSheetData, currentRowData)

'Read Data
summitCapitalUrlData = Trim(dataColumn("SUMMITCAPITAL_URL"))
selectMenuData = Trim(dataColumn("SELECT_MENU"))	
carPriceData = Trim(dataColumn("CAR_PRICE"))	
downPaymentData = Trim(dataColumn("DOWN_PAYMENT"))	
installmentDurationData = Trim(dataColumn("INSTALLMENT_DURATION"))
interestRatePerMonthData = Trim(dataColumn("INTEREST_RATE_PER_MONTH"))
installmentPerMonthData = Trim(dataColumn("INSTALLMENT_PER_MONTH"))
downPayment2Data = Trim(dataColumn("DOWN_PAYMENT_2"))	
installmentDuration2Data = Trim(dataColumn("INSTALLMENT_DURATION_2"))
interestRatePerMonth2Data = Trim(dataColumn("INTEREST_RATE_PER_MONTH_2"))

'Duplicate Data Header INPUT to OUPTUT
strSheetName = outputSheetData
DataTable.GetSheet(strSheetName).SetCurrentRow(MF_OUTPUT_ROW)
DataTable.Value("SUMMITCAPITAL_URL", strSheetName) = "'" & Trim(summitCapitalUrlData)
DataTable.Value("SELECT_MENU", strSheetName) = "'" & Trim(selectMenuData)
DataTable.Value("CAR_PRICE", strSheetName) = "'" & Trim(carPriceData)
DataTable.Value("DOWN_PAYMENT", strSheetName) = "'" & Trim(downPaymentData)
DataTable.Value("INSTALLMENT_DURATION", strSheetName) = "'" & Trim(installmentDurationData)
DataTable.Value("INTEREST_RATE_PER_MONTH", strSheetName) = "'" & Trim(interestRatePerMonthData)
DataTable.Value("INSTALLMENT_PER_MONTH", strSheetName) = "'" & Trim(installmentPerMonthData)
DataTable.Value("DOWN_PAYMENT_2", strSheetName) = "'" & Trim(downPayment2Data)
DataTable.Value("INSTALLMENT_DURATION_2", strSheetName) = "'" & Trim(installmentDuration2Data)
DataTable.Value("INTEREST_RATE_PER_MONTH_2", strSheetName) = "'" & Trim(interestRatePerMonth2Data)

'Create Capture Name
'MF_CAPTURE_NAME = "" & "_" & selectMenuData

MF_CAPTURE_NAME = "CAL_CREDIT"

'เปิดเว็บ SummitCapital
Call FW_OpenWebBrowserSUMMITCAPITAL("เปิดเว็บ SummitCapital", "summit capital , ซัมมิท", "summit capital , ซัมมิท", summitCapitalUrlData)
Wait 3
Call MaximizePageSUMMITCAPITAL("Internet Explorer")

Call FW_CaptureScreenShot(MF_CAPTURE_NAME, outputSheetData, MF_OUTPUT_ROW)

Call SUMMITCAPITALWEB_WebLink("กดเมนู คำนวณสินเชื่อ", "summit capital , ซัมมิท", "summit capital , ซัมมิท", "คำนวณสินเชื่อ_2")

Call SUMMITCAPITALWEB_WebEdit("ระบุราคารถ", "summit capital , ซัมมิท", "คำนวณสินเชื่อ summit capital", "ราคารถ", carPriceData)
Call SUMMITCAPITALWEB_WebEdit("ระบุเงินดาวน์ของยอดผ่อนต่อเดือน", "summit capital , ซัมมิท", "คำนวณสินเชื่อ summit capital", "เงินดาวน์_ยอดผ่อน", downPaymentData)

CALL SUMMITCAPITALWEB_WebList("ระบุระยะเวลาผ่อนชำระของยอดผ่อนต่อเดือน", "summit capital , ซัมมิท", "คำนวณสินเชื่อ summit capital", "ระยะเวลาผ่อนชำระ_ยอดผ่อน", installmentDurationData)
Call SUMMITCAPITALWEB_WebEdit("ระบุอัตราดอกเบี้ยต่อเดือนของยอดผ่อนต่อเดือน", "summit capital , ซัมมิท", "คำนวณสินเชื่อ summit capital", "อัตราดอกเบี้ยต่อเดือน_ยอดผ่อน", interestRatePerMonthData)

CALL SUMMITCAPITALWEB_WebButton("คลิกปุ่ม ตกลง ของยอดผ่อนต่อเดือน", "summit capital , ซัมมิท", "คำนวณสินเชื่อ summit capital", "ตกลง_ยอดผ่อน")

Call FW_CaptureScreenShot(MF_CAPTURE_NAME, outputSheetData, MF_OUTPUT_ROW)

Wait 1
  
InstallmentAmountPerMonth = Browser("summit capital , ซัมมิท").Page("คำนวณสินเชื่อ summit capital").WebEdit("ยอดผ่อนชำระต่อเดือน").GetROProperty("Value")
print InstallmentAmountPerMonth

DataTable.Value("INSTALLMENT_AMOUNT_PER_MONTH", strSheetName) = Trim(InstallmentAmountPerMonth)

If InstallmentAmountPerMonth <> "" Then
	Step1Check = True
Else
	Step1Check = False
End If

Call SUMMITCAPITALWEB_WebEdit("ระบุค่างวดต่อเดือน", "summit capital , ซัมมิท", "คำนวณสินเชื่อ summit capital", "ค่างวดต่อเดือน", installmentPerMonthData)
Call SUMMITCAPITALWEB_WebEdit("ระบุเงินดาวน์ของวงเงินสินเชื่อ", "summit capital , ซัมมิท", "คำนวณสินเชื่อ summit capital", "เงินดาวน์_วงเงินสินเชื่อ", downPayment2Data)

CALL SUMMITCAPITALWEB_WebList("ระบุระยะเวลาผ่อนชำระของวงเงินสินเชื่อ", "summit capital , ซัมมิท", "คำนวณสินเชื่อ summit capital", "ระยะเวลาผ่อนชำระ_วงเงินสินเชื่อ", installmentDuration2Data)
Call SUMMITCAPITALWEB_WebEdit("ระบุอัตราดอกเบี้ยต่อเดือนของวงเงินสินเชื่อ", "summit capital , ซัมมิท", "คำนวณสินเชื่อ summit capital", "อัตราดอกเบี้ยต่อเดือน_วงเงินสินเชื่อ", interestRatePerMonth2Data)

CALL SUMMITCAPITALWEB_WebButton("คลิกปุ่ม ตกลง ของวงเงินสินเชื่อ", "summit capital , ซัมมิท", "คำนวณสินเชื่อ summit capital", "ตกลง_วงเงินสินเชื่อ")

Call FW_CaptureScreenShot(MF_CAPTURE_NAME, outputSheetData, MF_OUTPUT_ROW)

Wait 1

HirePurchaseCredit = Browser("summit capital , ซัมมิท").Page("คำนวณสินเชื่อ summit capital").WebEdit("สินเชื่อเช่าซื้อ").GetROProperty("Value")
print HirePurchaseCredit

DataTable.Value("HIREPURCHASE_CREDIT", strSheetName) = Trim(HirePurchaseCredit)

If HirePurchaseCredit <> "" Then
	Step2Check = True
Else
	Step2Check = False
End If

If  Step1Check AND Step2Check Then
	DataTable.Value("TS_STATUS", strSheetName) = Trim("PASS")
	DataTable.Value("TS_MESSAGE", strSheetName) = Trim("")
Else
	DataTable.Value("TS_STATUS", strSheetName) = Trim("FAIL")
	DataTable.Value("TS_MESSAGE", strSheetName) = Trim("INVAILD VALUE")
End If

Call FW_CloseWebBrowserIE("ปิด Browser")

'Handle Error Verify
If MF_VERIFY = "Y" Then
	tsStatus = DataTable.Value("TS_STATUS", strSheetName)
	tsMessage = DataTable.Value("TS_MESSAGE", strSheetName)
	If tsStatus = "FAIL" Then
		MF_EXIT_TEST = "Y"
		Call FW_PrintInfo("micFail", tsStatus, " Expected Result : " & tsMessage)
	End If
End If @@ hightlight id_;_12060866_;_script infofile_;_ZIP::ssf28.xml_;_


